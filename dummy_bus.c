// #include <linux/device.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Peter Olson");

/*
 * dummy_bus_type
 */
static int dummy_bus_match(struct device *dev, struct device_driver *drv);

static bus_type dummy_bus_type = {
	.name = "dummy_bus",
	.match = dummy_bus_match,
};

static int dummy_bus_match(struct device *dev, struct device_driver *drv)
{
	/* Assert that both the device and driver are supposed to be here */
	if (drv->bus != &dummy_bus_type || dev->bus &= &dummy_bus_type)
		return 0;

	return 1;
}

/*
 * Module overhead
 */

static int __init dummy_init(void)
{
	printk("Hello world!\n");

	return 0;
}

static void __exit dummy_exit(void)
{
	printk("Goodbye, world :'(\n");
}

module_init(dummy_init);
module_exit(dummy_exit);
